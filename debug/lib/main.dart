import 'dart:async';

import 'package:flutter/material.dart';
import 'package:chat/view/chat_page.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:uuid/uuid.dart';

void main() {
  var id = 1;
  Stream<types.Message> onlineMessages = Stream.periodic(
    const Duration(seconds: 5),
    (_) {
      id++;
      return types.TextMessage(
        author: const types.User(
          id: "asdfasdf",
          firstName: "other",
          lastName: "people",
        ),
        text: "hello world" + id.toString(),
        createdAt: DateTime.now().millisecondsSinceEpoch,
        id: const Uuid().v4(),
      );
    },
  );
  MyApp app = MyApp();
  onlineMessages.listen((event) {
    app.messageStreamController.add(event);
  });
  runApp(app);
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final StreamController<types.Message> messageStreamController = StreamController<types.Message>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Test Chat Layout"),
        ),
        body: ChatPage(
          messageStream: messageStreamController.stream,
          onSendMessage: (message) {
            messageStreamController.add(message);
          },
        ),
      ),
    );
  }
}
