<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

This is the chat page module
include this module in your flutter project to use.

For chat page contributors: the debug folder is another flutter project that you can use
to see and debug this chat page app.

## Features

TODO: List what your package can do. Maybe include images, gifs, or videos.

## Getting started

To use this module, you would need 2 packages, [chat] (this module) and [flutter_chat_types]
```yml
dependencies:
  chat:
    path: ../
  flutter_chat_types: ^3.2.0
```

## Usage

TODO: Include short and useful examples for package users. Add longer examples
to `/example` folder.

The chat page accepts a stream of [Message]s (from flutter_chat_types: ^3.2.0).
This [Stream] of [Message] can be build from anywhere such as websocket, firebase, etc.
[onSendMessage] are called when the user of this phone sends a message, you should process
the message and feed it back into the stream so it can be showed in the page.

```dart
final StreamController<types.Message> messageStreamController = StreamController<types.Message>();

@override
void initState() {
  // source stream
  Stream<string> stream = ...
  stream.listen((event) {
    messageStreamController.add(event);
  });
}

@override
Widget build(BuildContext context) {
  return MaterialApp(
    title: 'Flutter Demo',
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: Scaffold(
      appBar: AppBar(
        title: const Text("Test Chat Layout"),
      ),
      // Here is the component
      body: ChatPage(
        messageStream: messageStreamController.stream,
        onSendMessage: (message) {
          // your code here...
          // ...

          //if success...then
          messageStreamController.add(message);
        },
      ),
    ),
  );
}
```

## Additional information