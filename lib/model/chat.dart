library chat;

import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class Chat {
  List<types.Message> messages = [];
  final user = const types.User(id: '06c33e8b-e835-4736-80f4-63f44b66666c');

  void addMessage(types.Message message) {
    messages.insert(0, message);
  }

  void clearMessage() {
    messages = [];
  }
}
