library chat;

import 'dart:async';

import 'package:chat/presenter/chat_presenter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';

/// Use [ChatPage] to open a chat page
class ChatPage extends StatefulWidget {
  final Stream<types.Message> messageStream;
  final Function(types.Message) onSendMessage;
  final StreamController<bool> _clearStream = StreamController<bool>();

  ChatPage({
    Key? key,
    required this.messageStream,
    required this.onSendMessage,
  }) : super(key: key);

  clearAllMessages() {
    _clearStream.add(true);
  }

  Stream<bool> get clearStream {
    return _clearStream.stream;
  }

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final ChatPresenter chatPresenter = ChatPresenter();

  @override
  void initState() {
    super.initState();
    widget.messageStream.listen((message) {
      setState(() {
        chatPresenter.handleIncomingMessage(message);
      });
    });
    widget.clearStream.listen((isClearing) {
      if (isClearing) {
        setState(() {
          chatPresenter.clearMessage();
        });
      }
    });
  }

  _handleMessageTap(types.Message message) {
    setState(() {
      chatPresenter.handleMessageTap(message);
    });
  }

  _onPreviewDataFetched(types.TextMessage message, types.PreviewData data) {
    setState(() {
      chatPresenter.handlePreviewDataFetched(message, data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Chat(
        messages: chatPresenter.chat.messages,
        onAttachmentPressed: () {
          setState(() {
            chatPresenter.handleAtachmentPressed(context);
          });
        },
        onMessageTap: _handleMessageTap,
        onPreviewDataFetched: _onPreviewDataFetched,
        onSendPressed: (types.PartialText text) {
          setState(() {
            types.Message message = chatPresenter.createMessage(text);
            widget.onSendMessage(message);
          });
        },
        user: chatPresenter.chat.user,
        showUserNames: true,
      ),
    );
  }
}
